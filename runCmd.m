function [status,result] = runCmd( cmd , varargin)

pth=which('callCmdFromMATLAB.sh');
if isempty(pth)
    error('Need to add callCmdFromMATLAB.sh to MATLAB path');
end

echoFlag = 0;
if mod(length(varargin),2) ~= 0
    error('Arguments must come in pairs');
end

% Defaults
%envString = 'env LD_LIBRARY_PATH=/usr/local/lib/:/usr/lib/x86_64-linux-gnu:${LD_LIBRARY_PATH} ';
% for speedy3
envString = 'env LD_LIBRARY_PATH=/usr/local/lib/:/usr/lib/x86_64-linux-gnu:/home/nrhineha/lib/opencv2.4/lib/:${LD_LIBRARY_PATH} ';

for i = 1:2:length(varargin)
    switch varargin{i}
        case 'echo'
            echoFlag = varargin{i+1};
            break;
        case 'envString'
            envString = varargin{i+1};
    end
end

if echoFlag
    disp(cmd);
%    [status,result] = system([envString cmd],'-echo');
%    [status,result] = system(sprintf([envString '\n' cmd]),'-echo');
    [status,result] = system(sprintf('sh %s %s',pth,cmd),'-echo');
else
    [status,result] = system(sprintf('sh %s %s',pth,cmd));
%     [status,result] = system(sprintf('sh callCmdFromMATLAB.sh %s',cmd));
%     [status,result] = system([envString cmd]);
end

end
