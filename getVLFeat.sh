#!/bin/bash

oldDir=${PWD}
VLFEATROOT="${PWD}/../submodules/vlfeat"

# Git clone vlfeat
if [ ! -d "../submodules/vlfeat" ]; then
git clone https://github.com/vlfeat/vlfeat.git ${VLFEATROOT}
fi

# Compile
cd ../submodules/vlfeat
make
cd ${oldDir}

# Run setup
vlfeatsetupscript="~/Documents/MATLAB/startup.m"
echo "Command to add to scripts: run('${VLFEATROOT}/toolbox/vl_setup')"
echo "I'm adding this line to ${vlfeatsetupscript}"
echo "run('${VLFEATROOT}/toolbox/vl_setup')" > ${vlfeatsetupscript}
matlab -nosplash -nodesktop -nojvm -nodisplay -r "run('${vlfeatsetupscript}'), quit"

# Test
echo "Testing VLFeat"
oldDir=${PWD}

matlab -nosplash -nodesktop -nojvm -nodisplay -r vl_version verbose
cd ${oldDir}
cd ../global/gentools/vlfeat/toolbox

