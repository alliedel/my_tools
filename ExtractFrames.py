import os, sys
import GetFrameRate
import subprocess
import argparse

class defaults:
    pathToFrames=None;
    fps=None;
    framestr='image-%06d.png';
    verbose=1;
    dry_run=0;


def ExtractFrames(pathToVideo, pathToFrames=defaults.pathToFrames, fps=defaults.fps, framestr=defaults.framestr, verbose=defaults.verbose, dry_run=defaults.dry_run):
    pathToVideo = os.path.expanduser(pathToVideo);

    if pathToFrames is None:
        if not os.path.isfile(pathToVideo):
            raise Exception('pathToVideo does not exist: ' + pathToVideo);

        (pth, fn_ext) = os.path.split(pathToVideo);
        (fn, ext) = os.path.splitext(fn_ext);
        pathToFrames = os.path.join(pth,fn + 'frames');
    pathToFrames = os.path.expanduser(pathToFrames);
    
    img1name = os.path.join(pathToFrames, framestr % (1));

    if os.path.isfile(img1name):
        if verbose:
            print( 'Frames previously extracted in {}'.format(pathToFrames))
        return pathToFrames;
    else:
        if verbose:
            print( 'Extracting frames to {}'.format(pathToFrames))


    if fps is None:
        fps = GetFrameRate.GetFrameRate(pathToVideo);

    if not os.path.isdir(pathToFrames):
        os.makedirs(pathToFrames);
    old_path = os.getcwd();
    if isinstance(fps, str):
        fps = float(fps);
    cmd = ('ffmpeg -i ' + pathToVideo + 
           ' -qscale 1 -r %.3g ' % (fps) +
           os.path.join(pathToFrames, framestr) );
    if verbose:
        print( cmd)
    if dry_run:
        print(cmd); return;
    out = subprocess.check_output(cmd, shell=True);
    
    if not os.path.isfile(img1name):
        display(out);
        raise Exception('ffmpeg did not extract the frames. Check the error message above.\n');
    return pathToFrames;

def ParseArguments():
    parser = argparse.ArgumentParser();
    parser.add_argument("pathToVideo",type=str);
    parser.add_argument("pathToFrames",type=str,nargs='?',help="defaults to pathToVideo + 'frames'");
    parser.add_argument("--fps",type=float,default=defaults.fps,nargs='?',help='Default: finds fps of the video');
    parser.add_argument("--framestr",type=str,default=defaults.framestr,nargs='?',help="[image-%%06d.png]");
    parser.add_argument("--verbose",type=int,default=defaults.verbose,nargs='?',help='[0],1');
    parser.add_argument("--dry_run", type=int, default=defaults.dry_run, nargs='?', help='[0],1');
    args = parser.parse_args();
    return args;

if __name__ == '__main__':
    args = ParseArguments();
    ExtractFrames(args.pathToVideo, args.pathToFrames, args.fps, args.framestr, args.verbose);
