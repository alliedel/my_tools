import os, sys
import argparse
import subprocess

class defaults:
    verbose=0;
    scale_flag=1;


def ResizeXY(pathToVideoIn, pathToVideoOut, resizex, resizey, scale_flag=defaults.scale_flag, verbose=defaults.verbose):
    (pathToVideoIn, pathToVideoOut, resizex,resizey) = CheckInputs(pathToVideoIn, pathToVideoOut, resizex, resizey, scale_flag);
    
    if scale_flag == 1:
        cmd = 'ffmpeg -i %s -vf scale=iw*%.3g:ih*%.3g %s' % (pathToVideoIn, resizex, resizey, pathToVideoOut);
    else:
        cmd = 'ffmpeg -i %s -vf scale=%.3g:%.3g %s' % (pathToVideoIn, resizex, resizey, pathToVideoOut);

    if verbose:
        print cmd;

    subprocess.call(cmd, shell=True);
    return;


def CheckInputs(pathToVideoIn, pathToVideoOut, scalex, scaley, scale_flag):
    pathToVideoIn = os.path.expanduser(pathToVideoIn);
    pathToVideoOut = os.path.expanduser(pathToVideoOut);

    if scale_flag == 0:
        if scalex is None:
            scalex = -1;
        if scaley is None:
            scaley = -1;
    else:
        if scalex is None:
            scalex = 1;
        if scaley is None:
            scaley = 1;

    if not os.path.isfile(pathToVideoIn):
        raise Exception('Path to video does not exist: ' + pathToVideoIn);

    outdir = os.path.dirname(pathToVideoOut);
    if (not os.path.exists(outdir)) and not len(outdir) == 0:
        os.makedirs(outdir);
    
    return (pathToVideoIn, pathToVideoOut, scalex, scaley);

def ParseArguments():
    parser = argparse.ArgumentParser();
    parser.add_argument("pathToVideoIn",type=str);
    parser.add_argument("pathToVideoOut",type=str);
    parser.add_argument("scalex",type=float, default=None);
    parser.add_argument("scaley",type=float, default=None);
    parser.add_argument("--scale_flag",type=int, nargs='?', default=defaults.scale_flag, help='[1]: numbers are multiplicative scales of iw,ih, 0: numbers are pixel dimensions (or -1 for current dimension)');
    parser.add_argument("--verbose",type=int, nargs='?', default=0,help='[0], 1');
    args = parser.parse_args();
    return args;

if __name__ == '__main__':
    args = ParseArguments();
    ResizeXY(args.pathToVideoIn, args.pathToVideoOut, args.scalex, args.scaley, args.scale_flag, args.verbose);
