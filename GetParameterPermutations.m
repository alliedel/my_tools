function parPerms = GetParameterPermutations(parsCombos)
% Forms parameter cell arrays or structures from the list of inputs.
% Example:
% 
% Input:
% parsCombos = struct('f1', {{'opt1','opt2'}}, 'f2', 4, 'f3', [1, 2])
% 
% Output:
% parPerms{1} = struct('f1', 'opt1', 'f2', 4, 'f3', 1)
% parPerms{2} = struct('f1', 'opt1', 'f2', 4, 'f3', 2)
% parPerms{3} = struct('f1', 'opt2', 'f2', 4, 'f3', 1)
% parPerms{3} = struct('f1', 'opt2', 'f2', 4, 'f3', 2)
if isempty(parsCombos)
    parPerms = {}; %#ok<NASGU>
end

assert(length(parsCombos)==1); % Perhaps you didn't enclose a cell array in brackets?  See the example above.

% Get number of parameter options for each parameter (field)
fields = fieldnames(parsCombos);
nElem = ones(size(fields));
ip = 1;

for fi = 1:length(fields)
    f = fields{fi};
    args = parsCombos.(f);
    if iscell(args)
        nElem(fi) = length(args);
    elseif ischar(args)
            nElem(fi) = 1;
            parsCombos.(f) = {parsCombos.(f)}; % Enclose strings in cell array for easier indexing
    elseif isnumeric(args)
        nElem(fi) = length(args);
    else
        error(['I don''t know what you''re passing in. ' ...
            'Feel free to comment out the code below this to make it ' ...
            'work if you know what you''re doing, or just enclose ' ...
            'your stuff in a cell array so I can ignore its type.\n'])
%     else
%         nElem(fi) = 1;
%         parsCombos.(f) = {parsCombos.(f)}; % Enclose in cell array for
%         easier indexing (I don't need to know what's inside).
    end
end


% Get all combinations
permInds = PermutationIndices(nElem);


% Index
nPerms = size(permInds,1);
nPars = size(permInds,2);
parPermsCell = cell(nPerms,1);
assert(nPars == length(fields));

firstCell = cell(1,2*nPars);
for fi = 1:nPars
    firstCell{2*fi-1} = fields{fi};
end

for p = 1:nPerms
    ca = firstCell;
    for fi = 1:nPars
        f = fields{fi};
        opts = parsCombos.(f);
        if iscell(opts)
            val = opts{permInds(p,fi)};
        else
            val = opts(permInds(p,fi));
        end
        
    % If you need a structure instead of a cell array
%         strct.(f) = val;
        ca{2*fi} = val;
    end
    parPermsCell{p} = ca;
end

parPerms = parPermsCell;

end



function perms = PermutationIndices(siz)
% TODO: Add option to determine order in which a is permuted through (b).
% Example:
%   a = [1, 1, 2, 1, 1, 4]
%   perms = ... 
%   [1, 1, 1, 1, 1, 1; ...
%    1, 1, 2, 1, 1, 1; ...
%    1, 1, 1, 1, 1, 2; ...
%    1, 1, 2, 1, 1, 2; ...
%    1, 1, 1, 1, 1, 3]
%    1, 1, 2, 1, 1, 3]
%    1, 1, 1, 1, 1, 4]
%    1, 1, 2, 1, 1, 4]
% My code is based on MATLAB's NDGRID, with some input/output tweaks.

n = length(siz);
p = prod(siz);

if length(siz) == 1
    perms = 1;
else
    perms = zeros(p,n);
    for i=1:n
        x = full(1:siz(i))'; % Extract and reshape as a vector.
        s = siz(:)'; s(i) = []; % Remove i-th dimension
        x = reshape(x(:,ones(1,prod(s))),[length(x) s]); % Expand x
        arr = permute(x,[2:i 1 i+1:n]); % Permute to i'th dimension
        perms(:,i) = arr(:);
    end
end

end
