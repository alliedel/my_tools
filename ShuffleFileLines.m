function ShuffleFileLines(inFile, outFile, shuffleIdxs)

res = cellfun(@(v) [v 'p;'], cellstr(num2str(shuffleIdxs(1:(end-1))'))', 'UniformOutput', 0);

linesStr = [res num2str(shuffleIdxs(end)) 'p'];
%     strcat(num2str(shuffleIdxs(2:end)),{'; p'})];

system(sprintf('rm -f %s',outFile));
cmd = [];
for i = 1:length(shuffleIdxs)
    idx = shuffleIdxs(i);
    cmd = [sprintf('sed -n ''') ...
        sprintf('%dp'' %s >> %s',idx,inFile, outFile)];
%     display(cmd)
    [stat, res] = system(cmd);
end
cmd(end-2:end) = [];
% display(cmd)

end