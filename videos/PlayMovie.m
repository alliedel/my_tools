function PlayMovie(outMov, framePause)

if nargin < 2
    framePause = 0.00;
end

%% Check movie format and adjust accordingly
if iscell(outMov)
    cellMov = outMov;
    nframes = length(cellMov);
%     % Reformat movie
%     sz = size(outMov{1});
%     outMov = zeros([sz nframes]);
%     for i = 1:nframes
%         outMov(:,:,:,i) = cellMov{i};
%     end
elseif isnumeric(outMov) && ndims(outMov) > 2
    if ndims(outMov) == 3
        % If ndim = 3, we assume each frame is 2-D.
        % Reformat so the movie is a 4-D matrix (3-channel grayscale).
        outMov = repmat(reshape(outMov,[size(outMov,1), size(outMov,2), 1, size(outMov,3)]),[1 1 3 1]);
    end
    nframes = size(outMov,4);
else
    error('Format of input variable outMov not recognized\n');
end


if(iscell(outMov))
    for framei=1:nframes
        imshow(outMov{framei}); drawnow;
        title(num2str(i))
        pause(framePause);
    end
else %isMatrix(outMov)
    outMov = outMov/max(max(max(max(outMov))));
    for framei=1:nframes
        imshow(outMov(:,:,:,framei)); drawnow;
        title(num2str(framei))
        pause(framePause);
    end
end
end
