function [ out_videoname ] = MergeFrames( pathToFrames, out_videoname, fps, imgString, pathToBashscript_FramesToVideo)
% [ out_videoname ] = MergeFrames( pathToFrames, out_videoname, fps, imgString, pathToBashscript_FramesToVideo)

%% Write frames to movie
[pth,nm,ext] = fileparts(out_videoname); if ~exist(pth, 'dir'); mkdir(pth); end;

old_path = pwd;
% cmd = ['ffmpeg -f image2 -i ', , sprintf(' -qscale 1 -r %d ',fps) ];
if exist(out_videoname,'file')
    system(sprintf('rm %s',AbsolutePath(fullfile(out_videoname))),'-echo');
end
if ~exist('imgString','var')
    imgString = 'image-%06d.jpg';
end
if ~exist('pathToBashscript_FramesToVideo','var')
    pathToBashscript_FramesToVideo = 'code/src/externaltools/my_tools/videos/framesToVideo.sh';
end
if ~exist(pathToBashscript_FramesToVideo,'file')
    error('please change pathToBashscript_FramesToVideo.  Must point to framesToVide.sh \n  It currently points to %s',pathToBashscript_FramesToVideo);
end

s_script = pathToBashscript_FramesToVideo;
s_imgs = AbsolutePath(fullfile(pathToFrames,imgString));
s_vidout = AbsolutePath(fullfile(out_videoname));
cmd = sprintf('sh %s %s %.3g %s', ...
    s_script, ['"' s_imgs '"'], fps, s_vidout);

system(cmd,'-echo');

end
