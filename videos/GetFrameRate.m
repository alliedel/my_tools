function fps = GetFrameRate(vidPath)

if ~exist(vidPath,'file')
    error('Video does not exist')
end

[stat,fps] = runCmd(['ffmpeg -i ' vidPath ' 2>&1 | sed -n "s/.*, \(.*\) fp.*/\1/p"'],'-echo',1);
fps = num2str(str2num(fps)); % get rid of terrible white space issues
if stat~=0 || isempty(fps)
    % Maybe it's avi?
    [stat,fps] = runCmd(['ffmpeg -i ' vidPath ' 2>&1 | sed -n "s/.*, \(.*\) tbr.*/\1/p"'],'-echo',1);

    if stat ~= 0 || isempty(fps)
        [stat,fps] = runCmd(['ffmpeg -i ' vidPath],'-echo',1);
        if stat ~= 0 || isempty(fps)
            error('ffmpeg couldn''t process %s',vidPath);
        else
            error('piping of ffmpeg results didn''t work :( \n If didn''t work with avi, try converting to mp4');
        end
    end
end

fps = str2num(fps);

end
