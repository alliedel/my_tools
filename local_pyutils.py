from contextlib import contextmanager
from functools import reduce  # forward compatibility for Python 3
from imp import reload
from functools import partial
from operator import eq

import copy
import collections
import datetime
import errno
import logging
import numpy as np
import operator
import os
import pickle
try:
    import psutil
except ImportError:
    psutil = None
import pytz
import shelve
import shlex
import shutil
import subprocess
import sys
import scipy
import typing
import yaml
from collections import OrderedDict

IS_PYTHON3 = True if sys.version_info >= (3, 0) else False
if IS_PYTHON3:
    import itertools
    imap = map
else:
    from itertools import imap


# Optional packages
try:
    from guppy import hpy
except ImportError:
#    print(Warning('Without guppy you\'ll be unable to support memory profiling'))
    pass


def flatten(d, parent_key='', sep='.'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def str2bool(v):
    """
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    """
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def get_class_attributes(cls, prune_hidden=True):
    return [d for d in dir(cls) if not callable(getattr(cls, d)) and d[0] != '_']

@contextmanager
def cd(newdir):
    """
    Example usage:
    os.chdir('/home')

    with cd('/tmp'):
        # ...
        raise Exception("There's no place like home.")
    """
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def nth_item(n, item, iterable):
    indicies = itertools.compress(itertools.count(), imap(partial(eq, item), iterable))
    return next(itertools.islice(indicies, n, None), -1)


def unique(x):
    if type(x) == np.ndarray:
        return np.unique(x)
    elif type(x) == list:
        return list(set(list(x)))
    else:
        raise TypeError('unique not implemented for type {}'.format(type(x)))


def crop_to_middle(matrix, out_shape):
    height = matrix.shape[0]
    width = matrix.shape[1]
    out_height = out_shape[0]
    out_width = out_shape[1]
    assert out_height <= height, ValueError('out_shape must be <= shape(X)')
    assert out_width <= width, ValueError('out_shape must be <= shape(X)')

    start_row = int(np.floor((height - out_height) / 2.0))
    start_col = int(np.floor((width - out_width) / 2.0))
    return matrix[start_row:(start_row + out_height), start_col:(start_col + out_width)]


class dotdictify(dict, object):
    def __init__(self, value=None):
        if value is None:
            pass
        elif isinstance(value, dict):
            for key in value:
                self.__setitem__(key, value[key])
        else:
            raise TypeError('expected dict')

    def __setitem__(self, key, value):
        if key is not None and '.' in key:
            myKey, restOfKey = key.split('.', 1)
            target = self.setdefault(myKey, dotdictify())
            if not isinstance(target, dotdictify):
                raise KeyError('cannot set "%s" in "%s" (%s)' % (restOfKey, myKey, repr(target)))
            target[restOfKey] = value
        else:
            if isinstance(value, dict) and not isinstance(value, dotdictify):
                value = dotdictify(value)
            dict.__setitem__(self, key, value)

    def __getitem__(self, key):
        if key is None or '.' not in key:
            return dict.__getitem__(self, key)
        myKey, restOfKey = key.split('.', 1)
        target = dict.__getitem__(self, myKey)
        if not isinstance(target, dotdictify):
            raise KeyError('cannot get "%s" in "%s" (%s)' % (restOfKey, myKey, repr(target)))
        return target[restOfKey]

    def __contains__(self, key):
        if key is None or '.' not in key:
            return dict.__contains__(self, key)
        myKey, restOfKey = key.split('.', 1)
        if not dict.__contains__(self, myKey):
            return False
        target = dict.__getitem__(self, myKey)
        if not isinstance(target, dotdictify):
            return False
        return restOfKey in target

    def setdefault(self, key, default):
        if key not in self:
            self[key] = default
        return self[key]

    def get(self, k, d=None):
        if dotdictify.__contains__(self, k):
            return dotdictify.__getitem__(self, k)
        return d

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def deepcopy(self):
        self_copy = dotdictify()
        for key, value in self.items():
            if isinstance(value, dotdictify):
                copied_value = value.deepcopy()
            else:
                try:
                    copied_value = copy.deepcopy(value)
                except:
                    try:
                        copied_value = copy.copy(value)
                        logging.warning('{} could not be deep copied'.format(value))
                    except:
                        copied_value = value
                        logging.warning('{} could not be deep OR shallow copied'.format(value))
            self_copy.__setitem__(key, copied_value)
        return self_copy

    def __ne__(self, other):
        return not self.__eq__(other)

    # __getstate__ and __setstate__ needed for pickling
    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, d):
        self.__dict__.update(d)

    # __dir__ needed to enable autocomplete
    def __dir__(self):
        return self.keys()

    __setattr__ = __setitem__
    __getattr__ = __getitem__


def prune_bool_tree_by_value(bool_tree, comparison_value=True, remove=False):
    """
    Removes or keeps only one value in the tree.  By default, removes all 'leaves' that are False.  With value=True, remove=True, will remove all leaves that are True.  With value=False, remove=False, will keep all leaves that are False. (same behavior iff leave are only False/True)
    """

    def should_prune(my_value_, comparison_value_, remove_):
        if remove_ and (my_value_ == comparison_value_):
            return True
        elif not remove_ and not (my_value_ == comparison_value_):
            return True
        else:
            return False

    def recursive_prune(bool_tree_, comparison_value_, remove_):
        key_list = list(bool_tree_.keys())
        for key in key_list:
            if isinstance(bool_tree_[key], dict):
                recursive_prune(bool_tree_[key], comparison_value_, remove_)
                if bool_tree_[key] == {}:  # Remove empty leaf dicts
                    bool_tree_.pop(key, None)
            else:
                # Remove/keep leaf nodes
                if should_prune(bool_tree_[key], comparison_value_, remove_):
                    bool_tree_.pop(key, None)

    if isinstance(bool_tree, dict):
        recursive_prune(bool_tree, comparison_value, remove)
        return bool_tree
    else:
        if should_prune(bool_tree, comparison_value, remove):
            return None
        else:
            return bool_tree


def get_recursive_key_list(dictionary, flattened=True):
    """ if flattened=False, returns 'hierarchical' list. Else returns list of length = number of nodes.
    To get a list of the keys as nested attributes, take the output, key_list, and run:
    (1)
    for key in key_list:
        print('.'.join(key))
    (2)
    for key in key_list:
        print('[\'{}\']'.format("\'][\'".join(key)))
    """
    if isinstance(dictionary, dict):
        key_list = []
        print('=== My list of keys: {} ==='.format(list(dictionary.keys())))
        print(len(list(dictionary.keys())))
        for i, key in enumerate(list(dictionary.keys())):
            lst = recurse_key_list(dictionary[key], flattened)
            if lst is not None:
                if flattened:
                    for item in lst:
                        if isinstance(item, list):
                            key_list.append([key] + item)
                        else:
                            key_list.append([key] + [item])
                else:
                    key_list.append(key)
                    key_list.append(lst)
            else:
                key_list.append(key)
        return key_list
    else:
        return None


def get_from_dict(dataDict, mapList):
    """ 
    Get nested items: getFromDict(dataDict, ["b", "v", "y"])
    """
    return reduce(operator.getitem, mapList, dataDict)


def set_in_dict(dataDict, mapList, value):
    """ 
    Set nested value: setInDict(dataDict, ["b", "v", "w"], 4
    """
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def compare_dotdicts(list_of_dotdicts):
    dotdict_orig = list_of_dotdicts[0]
    bool_trees = []
    for dotdict in list_of_dotdicts:
        bool_trees += [diff_items(dotdict, dotdict_orig)]
    return combine_bool_tree_list(bool_trees)

def my_and(a, b):
    return a and b


def nand(a,b):
    return a != b


def recursive_operation(item1, item2, operation):
    if isinstance(item1, dict) and isinstance(item2, dict):
        comparison=dict()
        for key in list(item1.keys()) + list(item2.keys()):
            try:
                comparison[key] = recursive_operation(item1[key], item2[key], operation)
            except:
                try:
                    comparison[key] = item1[key]
                except:
                    comparison[key] = item2[key]
        return comparison
    elif isinstance(item1, dict) or isinstance(item2, dict):
        return False
    else:
        assert type(item1) == bool
        assert type(item2) == bool
        return operation(item1, item2)


def combine_bool_tree_list(bool_tree_list, operation=my_and):
    """
    Concatenates list of bool trees (dotdicts or dicts) with 'or' or other specified operation.
    All keys that appeared in any of the bool trees will exist in the output.
    """
    combined_bool_tree = bool_tree_list[0]
    for i, bool_tree1 in enumerate(bool_tree_list[1:]):
        combined_bool_tree = recursive_operation(combined_bool_tree, bool_tree1, operation=operation)
    return combined_bool_tree
            

def diff_items(item1, item2):
    """
    returns tree structure, where each key in item1, item2 is included and whether they match or do not is labeled as True or False respectively
    """
    if isinstance(item1, dict) and isinstance(item2, dict):
        comparison=dict()
        for key in list(item1.keys()) + list(item2.keys()):
            try:
                comparison[key] = diff_items(item1[key], item2[key])
            except:
                comparison[key] = False
        return comparison
    elif isinstance(item1, dict) or isinstance(item2, dict):
         return False
    else:
        if item1 == item2:
            return True
        else:
            return False


class AttrDict(dict):
    def __init__(self, **kwargs):
        super(AttrDict, self).__init__(**kwargs)
        self.__dict__ = self


def replace_bad_filename_characters(string):
    string = string.replace(', ', 'x')
    string = string.replace('[', '')
    string = string.replace(']', '')
    return string


def argsort(seq):
    # http://stackoverflow.com/questions/3071415/efficient-method-to-calculate-the-rank-vector-of-a-list-in-python
    if type(seq) == list:
        return sorted(range(len(seq)), key=seq.__getitem__)
    if type(seq) == np.ndarray:
        return np.argsort(seq)
    else:
        raise NotImplementedError('argsort only implemented for lists and ndarrays')


def get_dictionary_hash_string(dictionary, excluded_keys=(), keyname_replacement_mappings={}):
    key_strings = []
    value_strings = []
    for key, value in dictionary.items():
        if key not in excluded_keys:  # List of parameters to leave out of filename go here
            key_strings.append(shortened_keyname(key, keyname_replacement_mappings))
            value_str = replace_bad_filename_characters(str(value))
            value_strings.append(value_str)
    sorted_idxs = argsort(key_strings)
    key_strings = [key_strings[idx] for idx in sorted_idxs]
    value_strings = [value_strings[idx] for idx in sorted_idxs]
    hash_str = '._'.join(['__'.join([key_str, value_str])
                          for key_str, value_str in zip(key_strings, value_strings)])
    return hash_str


def shortened_keyname(keyname, replacement_mappings):
    for substr, substr_replace in replacement_mappings.items():
        keyname = keyname.replace(substr, substr_replace)
    return keyname


def save_array(arr, filename):
    """
    Save an array to be loaded by python later. Here so we can make universal changes based
    on speed / readability later
    later.
    """
    # pickle.dump(arr, open(filename, 'w'))
    np.save(filename, arr)


def mkdir_if_needed(dirname):
    if os.path.isfile(dirname):
        raise ValueError('{} is an existing file!  Cannot make directory.'.format(dirname))
    if not os.path.isdir(dirname):
        os.mkdir(dirname)


def nans(shape, dtype=float):
    a = np.empty(shape, dtype)
    a.fill(np.nan)
    return a


# def get_logger(self, name=__name__):
#     logger = logging.getLogger(name)
#     if not len(logger.handlers):
#         logger = logging.getLogger(name)
#         logger.setLevel(logging.DEBUG)
#
#         handler = logging.StreamHandler(sys.stdout)
#         handler.setLevel(logging.DEBUG)
#         # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#         formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
#         handler.setFormatter(formatter)
#         logger.addHandler(handler)
#     return logger

def get_logger(name='my_logger', file=None):
    logger = logging.getLogger('my_logger')
    if not len(logger.handlers):
        logger.setLevel(logging.DEBUG)
        if file == None:
            file = '/tmp/my_log.log'
            print('Logging file in {}'.format(file))

        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

        fh = logging.FileHandler(file)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
    return logger


def replace_in_nested_dictionary(dictionary, key_to_replace, new_value):
    num_instances = replace_in_nested_dictionary_recurse(dictionary=dictionary,
                                                         key_to_replace=key_to_replace,
                                                         new_value=new_value)
    if num_instances == 0:
        raise ValueError('{} not found in {}'.format(key_to_replace, dictionary))
    if num_instances > 1:
        raise ValueError('{} found more than once in {}'.format(key_to_replace, dictionary))
    return num_instances


def replace_in_nested_dictionary_recurse(dictionary, key_to_replace, new_value):
    count = 0
    for key, value in dictionary.items():
        if isinstance(value, dict):
            count += replace_in_nested_dictionary_recurse(value, key_to_replace, new_value)
        elif key == key_to_replace:
            dictionary[key] = new_value
            count += 1
    return count


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def save_ipython_history_as_log(
        directory='/home/allie/Dropbox/academic/research/documentation/ipython_logs/',
                      prefix=None,
             label='', ext='.py', full_history=False, prompt_carrots=True,
        command_output=True):
    # Saves to <logdir>/<log_prefix_><log_label>.
    # By default, <prefix> is Y_m_d_H_M_S.
    # By default, <label> is ''.
    if prefix is None:
        prefix = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    if prefix and label:
        log_name = os.path.join(directory, prefix + '_' + label + ext)
    elif prefix:
        log_name = os.path.join(directory, prefix + ext)
    elif log_label:
        log_name = os.path.join(directory, label + ext)
    else:
        log_name = os.path.join(directory, 'default' + ext)

    flags = (' -g' if full_history else '') +\
            (' -p' if prompt_carrots else '') +\
            (' -o' if command_output else '')
    print('%history {} -f {}'.format(flags, log_name))
    get_ipython().magic('%history {} -f {}'.format(flags, log_name))
    return log_name


def free_memory_after_allocating_variable(variable_instance, num_copies=1):
    """
    A way to check whether the system has enough memory to allocate a variable (returns positive
    value) or does not (returns negative value)
    """
    if psutil is None:
        raise ImportError('You need to install psutil to check memory')
    virtual_memory = psutil.virtual_memory()
    free_memory_after_variable = virtual_memory.free - sys.getsizeof(variable_instance)
    return free_memory_after_variable


def is_sorted(array_like, decreasing=False):
    if decreasing:
        return all([x1 - x2 >= 0 for x1, x2 in zip(array_like[:-1], array_like[1:])])
    else:
        return all([x2 - x1 >= 0 for x1, x2 in zip(array_like[:-1], array_like[1:])])

"""
def type_hinting_example(name: str = 'Allie', repeat: int = 1) -> str:
    output_string = ''
    for i in range(repeat):
        output_string += 'Hello ' + name
    return output_string
"""

def shelve_session(filename='shelve.out'):
    if os.path.exists(filename):
        shutil.copyfile(filename, filename + '.backup')
        os.remove(filename)
    try:
        my_shelf = shelve.open(filename,'n') # 'n' for new

        for key in dir():
            try:
                my_shelf[key] = globals()[key]
            except TypeError:  # Allie added KeyError.  Don't think it's good, but couldn't
                #  get it to work otherwise...
                #
                # __builtins__, my_shelf, and imported modules can not be shelved.
                #
                print('ERROR shelving: {0}'.format(key))
            except KeyError:
                print('KeyError shelving: {}'.format(key))
        my_shelf.close()
    except:
        shutil.copy_file(filename + '.backup', filename)
        raise


def unshelve_session(filename='shelve.out'):
    my_shelf = shelve.open(filename)
    for key in my_shelf:
        globals()[key]=my_shelf[key]
    my_shelf.close()


def unique_rows(arr):
    unique_rows = np.vstack({tuple(row) for row in np.vstack([arr, unique_colors])})


def index_to_onehot(y_indexed, n_classes):
    y_onehot = np.zeros(y_indexed.shape + (n_classes,))
    for cls in range(n_classes):
        y_onehot[..., cls] = y_indexed == cls
    return y_onehot


def index_list(lst, indices):
    """
    Does the list comprehension as a function instead. Less annoying.
    """
    return [lst[i] for i in indices]


def invert_permutation(perm):
    inverse = [0] * len(perm)
    print(perm)
    for i, p in enumerate(perm):
        inverse[int(p)] = i
    return inverse


def git_hash():
    cmd = 'git log -n 1 --pretty="%h"'
    hash = subprocess.check_output(shlex.split(cmd)).strip()
    if type(hash) is not str:
        hash = hash.decode('UTF-8')
    return hash


def get_log_dir(basename, config_dictionary=None, additional_tag='', set_up_dir=True):
    basedir = os.path.dirname(basename)
    basename = os.path.basename(basename)
    name = basename
    if len(name) > 0 and name is not None:
        name += '_'

    now = datetime.datetime.now(pytz.timezone('America/New_York'))
    name += '%s' % now.strftime('%Y-%m-%d-%H%M%S')
    name += '_VCS-%s' % git_hash()

    # load config
    if config_dictionary is not None:
        for k, v in config_dictionary.items():
            v = str(v)
            if '/' in v:
                continue
            if len(name) > 0 and name[-1] != '_':
                name += '_'
            name += '%s-%s' % (k.upper(), v)
    name += additional_tag

    # create out
    log_dir = os.path.join(basedir, name)
    if set_up_dir:
        logdir_setup(config_dictionary, log_dir)
    return log_dir


def logdir_setup(config_dictionary, log_dir, exist_ok=True):
    if not os.path.exists(log_dir):
        os.makedirs(log_dir, exist_ok=exist_ok)
    if config_dictionary is not None:
        with open(os.path.join(log_dir, 'config.yaml'), 'w') as f:
            try:
                yaml.safe_dump(config_dictionary, f, default_flow_style=False)
            except yaml.representer.RepresenterError:
                yaml.safe_dump(dict(config_dictionary), f, default_flow_style=False)
                print(Warning('converted config dictionary to non-ordereddict when saving'))


def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def get_memory_usage():
    if not hpy:
        raise Exception('External module {} missing'.format(hpy))
    return hpy().heap()


def get_variable_sizes():
    var_size_tuples = []
    for var, obj in locals().items():
        var_size_tuples.append((var, sys.getsizeof(obj)))

    return var_size_tuples


def flatten_dict(d, parent_key='', sep='/'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


class TermColors:
    """
    https://stackoverflow.com/questions/287871/print-in-terminal-with-colors
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def check_clean_work_tree(exit_on_error=False, interactive=True):
    child = subprocess.Popen(['git', 'diff', '--name-only', '--exit-code'], stdout=subprocess.PIPE)
    stdout = child.communicate()[0]
    exit_code = child.returncode
    if exit_code != 0:
        override = False
        if interactive:
            override = 'y' == input(
                TermColors.WARNING + 'Your working directory tree isn\'t clean:\n ' + TermColors.ENDC +
                TermColors.FAIL + '{}'.format(stdout.decode()) + TermColors.ENDC +
                'Please commit or stash your changes. If you\'d like to run anyway,\n enter \'y\': '
                '' + TermColors.ENDC)
        if exit_on_error or interactive and not override:
            raise Exception(TermColors.FAIL + 'Exiting.  Please commit or stash your changes.' + TermColors.ENDC)
    return exit_code, stdout
