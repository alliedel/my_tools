import os, sys
import subprocess
import argparse

class defaults:
    framestr='image-%06d.png';
    verbose=0;
    overwrite=-1;
    dry_run=False


def MergeFrames(pathToVideo, pathToFrames, fps, framestr=defaults.framestr,
                verbose=defaults.verbose, overwrite=defaults.overwrite,
                dry_run=defaults.dry_run):
    pathToVideo = os.path.expanduser(pathToVideo);
    pathToFrames = os.path.expanduser(pathToFrames);
    
    if verbose:
        if os.path.isfile(pathToVideo):
            print('Overwriting %s' % (pathToVideo))
        else:
            print('Creating %s' % (pathToVideo))

    if isinstance(fps, str):
        fps = float(fps);
    overwritestr = '';
    if overwrite == 1:
        overwritestr = ' -y ';
    elif overwrite == 0 and os.path.isfile(pathToVideo):
        if verbose:
            print('pathToVideo already exists: ' + pathToVideo)
        return
    # Note: added the -vf command to handle an ffmpeg bug
    cmd = ('ffmpeg ' + overwritestr + '-r %.3g -i ' % (fps) +
           os.path.join(pathToFrames, framestr) + 
           ' -vf scale="trunc(iw/2)*2:trunc(ih/2)*2" ' +  pathToVideo);
    if verbose:
        print('cmd:')
        print(cmd)
    if not dry_run:
        out = subprocess.check_output(cmd, shell=True);
        if not os.path.isfile(pathToVideo):
            display(out);
            raise Exception('ffmpeg did not merge the frames. Check the error message above.\n');
    return


def ParseArguments():
    parser = argparse.ArgumentParser();
    parser.add_argument("pathToVideo", type=str);
    parser.add_argument("pathToFrames", type=str);
    parser.add_argument("fps", type=float);

    parser.add_argument("--dryrun", type=int, default=defaults.dry_run, nargs='?');
    parser.add_argument("--framestr", type=str, default=defaults.framestr, nargs='?');
    parser.add_argument("--verbose", type=int, default=defaults.verbose, nargs='?');
    parser.add_argument("--overwrite", type=int,default=defaults.overwrite,nargs='?');
    args = parser.parse_args();
    return args;
    
if __name__ == '__main__':
    args = ParseArguments();
    MergeFrames(args.pathToVideo, args.pathToFrames, args.fps, args.framestr, args.verbose, args.overwrite);
