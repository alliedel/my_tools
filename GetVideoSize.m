function [M,N,T] = GetVideoSize(pathToVideo)
% [M,N,T] = GetVideoSize(pathToVideo) -- warning: must extract frames from
% the video.
    outdir = extractFrames(pathToVideo);
    frms = dir2cell_old(fullfile(outdir,'*.png'));
    if isempty(frms)
        frms = dir2cell_old(fullfile(outdir,'*.jpg'));
    end
    firstframe = frms{1};
    [M,N,~] = size(imread(firstframe));
    T = length(frms);

end
