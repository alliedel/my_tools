function out = myndgrid(siz)
%Based on MATLAB's NDGRID, with some input/output tweaks.

n = length(siz);

out = zeros(prod(siz),n);

% Get the size of the matrix so we can preallocate it
for i = 1
    x = full(1:siz(i))'; % Extract and reshape as a vector.
    s = siz; s(i) = []; % Remove i-th dimension
    x = reshape(x(:,ones(1,prod(s))),[length(x) s]); % Expand x
    arr = permute(x,[2:i 1 i+1:n]); % Permute to i'th dimension
    arr = arr(:);
end

p = length(arr);

out = zeros(p,n);
for i=1:n
    x = full(1:siz(i))'; % Extract and reshape as a vector.
    s = siz; s(i) = []; % Remove i-th dimension
    x = reshape(x(:,ones(1,prod(s))),[length(x) s]); % Expand x
    arr = permute(x,[2:i 1 i+1:n]); % Permute to i'th dimension
    out(:,i) = arr(:);
end
