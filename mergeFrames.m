function mergeFrames(pathToVideo, indir, fps, framestr)
%% Merge frames
% framestr is something like image-%06.png

[p,~,~] = fileparts(pathToVideo);
if ~exist(p,'dir')
    mkdir(p)
end

% example: ffmpeg -r 60 -f image2 -s 1920x1080 -i pic%04d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p test.mp4
%  -qscale 1
cmd = ['ffmpeg ' sprintf('-r %.3g ',fps), ... %'-preset medium -crf 22 ' ...
    sprintf('-i %s ', fullfile(indir,framestr)) ...
    pathToVideo];
disp(cmd);
[stat,out] = runCmd(cmd,'-echo',1);

if ~exist(pathToVideo,'file')
    error('ffmpeg did not merge the frames.  Check the error message above.\n')
end

end
