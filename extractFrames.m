function outdir = extractFrames(pathToVideo, outdir, fps)
imgext = '.png';
%% Extract frames

if ~exist('outdir','var') || isempty(outdir)
    [pth,fn,ext] = fileparts(pathToVideo);
    outdir = fullfile(pth,[fn 'frames']);
end

if ~exist('fps','var') || isempty(fps)
    fps = GetFrameRate(pathToVideo);
end

if exist(fullfile(outdir,sprintf(['image-%06d' imgext],1)),'file')
    fprintf('Frames previously extracted in %s\n',outdir)
    return;
else
    fprintf('Extracting frames to %s\n',outdir)
end
if ~exist(outdir,'dir'); mkdir(outdir); end
old_path = pwd;
if ischar(fps)
    fps = str2num(fps);
end
cmd = ['ffmpeg -i ' pathToVideo sprintf(' -qscale 1 -r %.3g ',fps), ...
    fullfile(outdir,'image'), '-%06d' imgext];
disp(cmd);
[stat,out] = runCmd(cmd,'-echo',1);

if ~exist(fullfile(outdir,sprintf(['image-%06d' imgext],1)),'file')
    error('ffmpeg did not extract the frames.  Check the error message above.\n')
end

end
