function [h,w,f,dur,fps] = GetMovieSz(vidPath)

v = VideoReader(vidPath);

h = v.Height;
w = v.Width;
f = v.NumberOfFrames;
dur = v.Duration;
fps = v.FrameRate;

end
