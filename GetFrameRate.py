import os,sys
import re
import subprocess
import argparse

class defaults:
    verbose=0;

def GetFfmpegLocation():
    hostname = str(subprocess.check_output('hostname', shell=True))
    if hostname.find("shona.ius.cs.cmu.edu") != -1:
        return "/usr4/imisra/research/common-code/opencv-static/bin/ffmpeg"
    else:
        return "ffmpeg"
    
    
def GetFrameRate(pathToVideo, verbose=defaults.verbose): # returns fps
    pathToVideo = os.path.expanduser(pathToVideo); # Handles the tilde
    if not os.path.isfile(pathToVideo):
        raise Exception('Video does not exist: ' + pathToVideo);
    ffmpeg_location = GetFfmpegLocation()
    cmd = ffmpeg_location + ' -i ' + pathToVideo
    try:
        cmd_ffmpeg_help=ffmpeg_location + ' --help'
        subprocess.check_output(cmd_ffmpeg_help, shell=True)
    except:
        raise SystemError('ffmpeg not found. Tried {}'.format(cmd_ffmpeg_help))
    try:
        res = str(subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True))
    except subprocess.CalledProcessError as ex:
        res = str(ex.output)
    res_options = res.split(',')
    for option in res_options:
        fps_location = option.find('fps')
        if fps_location != -1:
            res = option[:(fps_location-1)]
            break;
    if 0:
        cmd = 'ffmpeg -i ' + pathToVideo + ' 2>&1 | sed -n "s/.*, \(.*\) fp.*/\\1/p"';
        if verbose:
            print( 'Running ffmpeg command:')
            print(cmd)
            res = subprocess.check_output(cmd, shell=True);
            print('res: {}'.format(res))
        if res is None or len(res) == 0 or res == "":
            # sometimes this works instead (avi?)
            cmd='ffmpeg -i ' + pathToVideo + ' 2>&1 | sed -n "s/.*, \(.*\) tbr.*/\\1/p"';
            print( cmd);
            res = subprocess.check_output(cmd,shell=True);
            if res is None or res == 0.0 or res == "":
                cmd='ffmpeg -i ' + pathToVideo;
                print( cmd);
                ffmpeg_simple_res = subprocess.check_output(cmd,shell=True);
                print('Inside simple_res')
                if ffmpeg_simple_res == "":
                    raise Exception('ffmpeg did not work: ffmpeg -i ' + pathToVideo);
                else:
                    raise Exception('parsing of ffmpeg results didn''t work :(\n');
    fps = float(res);
    return fps;

def ParseArguments():
    parser = argparse.ArgumentParser();
    parser.add_argument("pathToVideo",type=str);
    parser.add_argument("--verbose",type=str,default=defaults.verbose,help="[0],1");
    args = parser.parse_args();
    return args;

if __name__ == '__main__':
    args = ParseArguments();
    fps = GetFrameRate(args.pathToVideo, args.verbose);
    print(fps)
