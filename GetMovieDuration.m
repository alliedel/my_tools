function nSeconds = GetMovieDuration(vidPath)

[stat1,res1] = runCmd( sprintf(['ff=$(ffmpeg -i ', vidPath, ' 2>&1)']));
[stat2,res2] = runCmd('echo $ff');
[stat2,res2] = runCmd('d="${ff#*Duration: }"');
[stat,hhmmssms] = runCmd('echo "${d%%,*}"');

end
