function [ absPath ] = AbsolutePath( relPath )

if ~exist(relPath,'dir')
    absPath = relPath;
    return
end

curDir = pwd;
[pathstr, nm, ext] = fileparts(relPath);
cd(pathstr); absFolder = pwd; cd(curDir);
absPath = fullfile(absFolder, [nm ext]);

end

