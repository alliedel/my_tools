function ret = existAndNotEmpty(fname)
MINSIZE=21;
if ~exist(fname,'file')
    ret=0;
else
    s = dir(fname);
    if s.bytes<MINSIZE
        ret=0;
        runCmd(sprintf('rm %s',fname),'-echo',1);
    else
        ret=1;
    end
end

end
