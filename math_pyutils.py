import numpy as np
import scipy.linalg


def orthogonality_matrix(V):
    """
    If V is nxm, returns an mxm matrix representing orthogonality between each pair of vectors.
    """
    n = V.shape[1]
    res = np.zeros((n,n))
    col_norms = np.linalg.norm(V, axis=0)
    for i in range(n):
        res[i,:] = V[:,i].dot(V) / (np.linalg.norm(V[:,i]) *
                                    col_norms)
    return res

def orthogonality_matrix_in_A(V, A):
    """
    If V is nxd, returns an nxn matrix representing orthogonality between each pair of vectors.
    """
    U,S,_ = np.linalg.svd(A)
    sqrtA = U.dot(np.diag(np.sqrt(S)))
    assert np.allclose(sqrtA.dot(sqrtA.T),A), ValueError('A must be symmetric.')
    sqrtAV = (V.T.dot(sqrtA)).T
    return orthogonality_matrix(sqrtAV)


def make_symmetric_with_sqrt(A):
    U,S,V = np.linalg.svd(A)
    sqrtA = U.dot(np.diag(np.sqrt(S)))
    assert np.allclose(sqrtA.dot(sqrtA.T),A), ValueError('A must be symmetric.')
    return sqrtA.dot(sqrtA.T)


def orthogonality(v1, v2):
    """
    Returns 1 if parallel; 0 if orthogonal.
    """
    return v1.dot(v2) / np.linalg.norm(v1) / np.linalg.norm(v2)


def orthogonality_in_A(v1, v2, A):
    """
    Returns 1 if parallel in A; 0 if orthogonal.
    Only works for symmetric A.
    """
    U,S,V = np.linalg.svd(A)
    sqrtA = U.dot(np.diag(np.sqrt(S)))
    assert np.allclose(sqrtA.dot(sqrtA.T),A), ValueError('A must be symmetric.')
    sqrtAv1 = v1.dot(sqrtA)
    sqrtAv2 = v2.dot(sqrtA)
    return orthogonality(sqrtAv1, sqrtAv2)


def nullspace_and_span(matrix, tol=None):
    U, S, Vt = scipy.linalg.svd(matrix)
    rank = np.linalg.matrix_rank(matrix, tol=tol) # as far as I can tell, rank(S) same as rank(
    # matrix)
    nullsp = Vt.T[:, rank:]
    spn = Vt.T[:, :rank]
    return nullsp, spn

def nullspace(matrix, tol=None):
    U, S, Vt = scipy.linalg.svd(matrix)
    rank = np.linalg.matrix_rank(matrix, tol=tol)
    nullsp = Vt.T[:, rank:]
    return nullsp

def span(matrix, tol=None):
    U, S, Vt = scipy.linalg.svd(matrix)
    rank = np.linalg.matrix_rank(matrix, tol=tol)
    spn = Vt.T[:, :rank]
    return spn


def gram_schmidt(vectors):
    return gram_schmidt_with_qr(vectors)


def gram_schmidt_with_qr(vectors):
    Q, R = np.linalg.qr(vectors)
    return Q


def gram_schmidt_with_projections(vectors, tol=1e-10, return_kept_locations=False,
                              auto_raise_tol=False):
    vectors = np.array(vectors)
    basis = []
    kept_locations = np.zeros(np.array(vectors).shape[1])
    for idx, v in enumerate(vectors.T):
        w = v - np.sum( np.dot(v,b)*b  for b in basis )
        if (np.abs(w) > tol).any():
            basis.append(w/np.linalg.norm(w))
            kept_locations[idx] = 1

    # Recursively increase the tolerance until we get a matrix with no nullspace
    if len(basis) > vectors.shape[0]:
        print(Warning('Generated more vectors than the dimensionality.  Numerically, something is '
                      'wrong.  Rerunning with higher tolerance.'))
        if auto_raise_tol:
            new_tol = tol * 10
            print('Setting tol to {}'.format(new_tol))
            return gram_schmidt(vectors, tol=new_tol, return_kept_locations=return_kept_locations,
                                auto_raise_tol=auto_raise_tol)
    else:
        if not return_kept_locations:
            return np.array(basis).T
        else:
            return np.array(basis).T, kept_locations == 1


def get_basis_similarity_matrix(basis1, basis2, eigenvalues1=None, eigenvalues2=None):
    """
    :param basis1: matrix with orthonormal column vectors
    :param basis2: matrix with orthonormal column vectors
    :param eigenvalues1: vector containing the corresponding eigenvalues; will scale the
    basis elements accordingly
    :param eigenvalues2: vector containing the corresponding eigenvalues; will scale the
    basis elements accordingly
    :return: returning a vector with all 1's means they're identical.  Returning a vector with
    all 0's means they're orthogonal.
    """

    if eigenvalues1 is None and eigenvalues2 is None:
        eigenvalues1 = np.ones((basis1.shape[1],))
        eigenvalues2 = np.ones((basis2.shape[1],))
    elif not eigenvalues1 or not eigenvalues2:
        raise ValueError('either none or both of eigenvalues1 and 2 must be specified')

    projected_values = np.empty(basis1.shape[0])
    projected_values1 = np.empty(basis1.shape[0])
    projected_values2 = np.empty(basis1.shape[0])
    basis2_scaled = np.diag(eigenvalues2).dot(basis2)
    for i, eigenvector in enumerate(basis1.T):
        projected_values2[i] = np.sum(eigenvector.dot(basis2_scaled))
    basis1_scaled = np.diag(eigenvalues1).dot(basis1)
    for i, eigenvector in enumerate(basis1.T):
        projected_values1[i] = np.sum(eigenvector.dot(basis1_scaled))

    return projected_values2

    # plt.plot(projected_values1, label='1'); plt.plot(projected_values2, label='2')
    # plt.legend()
    # plt.show()


def column_norms(matrix):
    return np.linalg.norm(matrix, ord=2, axis=0)


def squared_distance(x, y):
    return np.inner(x - y, x - y)


def compute_pairwise_distances(X):
    # Computes pairwise distances between points (each row in X).
    # Returns a n x n matrix, where X is n x m
    n = X.shape[0]
    D = np.zeros((n, n))
    for i in range(X.shape[0]):
        for j in range(X.shape[0]):
            D[i, j] = squared_distance(X[i, :], X[j, :])
            D[j, i] = D[i, j]

    return D


def zca_whiten(X, epsilon=1e-5):
    """
    Applies ZCA whitening to the data (X)
    http://xcorr.net/2011/05/27/whiten-a-matrix-matlab-code/

    X: numpy 2d array
        input data, rows are data points, columns are features

    Returns: ZCA whitened 2d array
    """
    Xc = X - np.mean(X, axis=0)
    #   covariance matrix
    cov = np.dot(Xc.T, Xc)

    d, E = np.linalg.eigh(cov)
    #   D = diag(d) ^ (-1/2)
    D = np.diag(1. / np.sqrt(d + epsilon))
    #   W_zca = E * D * E.T
    W = np.dot(np.dot(E, D), E.T)

    X_white = np.dot(Xc, W)

    return X_white


def assert_eigenvector_reconstruction(A, B, eigenvalues, eigenvectors):
    try:
        # Vector-scalar form
        assert np.all(
            [np.allclose(A.dot(eigenvectors[:, i]), eigenvalues[i] * B.dot(eigenvectors[:, i]))
             for i in range(eigenvectors.shape[0])])
        # Matrix-vector form
        assert np.allclose(A.dot(eigenvectors), B.dot(eigenvectors).dot(np.diag(eigenvalues)))
    except:
        raise AssertionError("Eigenvectors don\'t match reconstruction")


def assert_eigenvalues_match_objective(A, B, eigenvalues, eigenvectors):
    objective_values = [eigenvectors[:, col].dot(A).dot(eigenvectors[:, col]) /
                        eigenvectors[:, col].dot(B).dot(eigenvectors[:, col])
                        for col in range(len(eigenvalues))]
    try:
        assert np.allclose(objective_values, eigenvalues)
    except:
        AssertionError("Eigenvalues don\'t match objective values")



def princomp(A):
    """ performs principal components analysis
        (PCA) on the n-by-p data matrix A
        Rows of A correspond to observations, columns to variables.
        From: http://glowingpython.blogspot.sg/2011/07/principal-component-analysis-with-numpy.html

    Returns :
     coeff :
       is a p-by-p matrix, each column containing coefficients
       for one principal component.
     score :
       the principal component scores; that is, the representation
       of A in the principal component space. Rows of SCORE
       correspond to observations, columns to components.
     latent :
       a vector containing the eigenvalues
       of the covariance matrix of A.
    """
    # computing eigenvalues and eigenvectors of covariance matrix
    M = (A - np.mean(A.T, axis=1)).T  # subtract the mean (along columns)
    [latent, coeff] = np.linalg.eig(np.cov(M))  # attention:not always sorted
    score = coeff.T.dot(M)  # projection of the data in the new space
    return coeff, score, latent



def princomp(A):
    """ performs principal components analysis
        (PCA) on the n-by-p data matrix A
        Rows of A correspond to observations, columns to variables.
        From: http://glowingpython.blogspot.sg/2011/07/principal-component-analysis-with-numpy.html

    Returns :
     coeff :
       is a p-by-p matrix, each column containing coefficients
       for one principal component.
     score :
       the principal component scores; that is, the representation
       of A in the principal component space. Rows of SCORE
       correspond to observations, columns to components.
     latent :
       a vector containing the eigenvalues
       of the covariance matrix of A.
    """
    # computing eigenvalues and eigenvectors of covariance matrix
    M = (A - np.mean(A.T, axis=1)).T  # subtract the mean (along columns)
    [latent, coeff] = np.linalg.eig(np.cov(M))  # attention:not always sorted
    score = coeff.T.dot(M)  # projection of the data in the new space
    return coeff, score, latent


def is_diagonal(matrix, tol=1e-16):
    # Multiplying the matrix by itself should be equivalent to squaring its diagonal elements.
    are_zero = abs(matrix) <= tol
    are_zero[np.diag_indices_from(are_zero)] = True
    return np.all(are_zero)


def compute_covariance_matrix(X, return_mu=False):
    # X is nxd, where n is the number of points and d is feature dimensionality
    # Returns a dxd covariance matrix (and mu, if return_mu == True)

    n = np.shape(X)[0]
    mu = np.mean(X, axis=0)
    C = 1.0 / n * (X - mu).T.dot(X - mu)

    if return_mu:
        return C, mu
    else:
        return C
