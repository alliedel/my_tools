function htmlstring = saveAndDisplayFig(h, www_dir_web, www_dir_local, www_dir_local_repo)
if ~exist('www_dir_web','var')
    www_dir_web = WWW_DIR_WEB;
end
if ~exist('www_dir_local','var')
    www_dir_local = WWW_DIR_LOCAL;
end
if ~exist('www_dir_repo','var')
    www_dir_local_repo = WWW_DIR_LOCAL_REPO;
end
finum = 1;
figfilename = sprintf('%09d.png',finum);
%    figfilename_fig = sprintf('%09d.fig',finum);
while(exist(fullfile(www_dir_local_repo,figfilename),'file') || exist(fullfile(www_dir_local,figfilename),'file'))
    finum = finum + 1;
    figfilename = sprintf('%09d.png',finum);
    figfilename_fig = sprintf('%09d.fig',finum);
end
export_fig(h,fullfile(www_dir_local,figfilename));
www_figname = fullfile(www_dir_web, figfilename);
%%
htmlstring = sprintf('<html><img vspace="5" hspace="5" src=%s alt=""></html>\n',www_figname);
%    disp(htmlstring)
%%
end

function out = WWW_DIR_WEB
out = 'http://www.cs.cmu.edu/afs/cs/user/adelgior/www/research_results/www_figs/';
end
function out = WWW_DIR_LOCAL
out = '~/Documents/anomalydetect/data/www_figs/newfigs/';
end
function out = WWW_DIR_LOCAL_REPO
out = '~/Documents/anomalydetect/data/www_figs/';
end

