function htmlstring = copyAndDisplayFig(inname,   www_dir_web, www_dir_local)
% htmlstring = copyAndDisplayFig(inname) -- uses default paths for www_dirs
% htmlstring = copyAndDisplayFig(inname,   www_dir_web, www_dir_local)
if ~exist('www_dir_web','var')
    www_dir_web = WWW_DIR_WEB;
end
if ~exist('www_dir_local','var')
    www_dir_local = WWW_DIR_LOCAL;
end
if ~exist('www_dir_repo','var')
    www_dir_repo = WWW_DIR_REPO;
end

finum = 1;
figfilename = sprintf('%09d.png',finum);
while(exist(fullfile(WWW_DIR_LOCAL_REPO,figfilename),'file') || exist(fullfile(WWW_DIR_LOCAL,figfilename),'file'))    finum = finum + 1;
    figfilename = sprintf('%09d.png',finum);
end
cmd = sprintf('cp %s %s',inname,fullfile(www_dir_local,figfilename));
system(cmd);
www_figname = fullfile(www_dir_web, figfilename);
%%
htmlstring = sprintf('<html><img vspace="5" hspace="5" src=%s alt=""></html>\n',www_figname);
%%
end


function out = WWW_DIR_WEB
out = 'http://www.cs.cmu.edu/afs/cs/user/adelgior/www/research_results/www_figs/';
end
function out = WWW_DIR_LOCAL
out = '~/Documents/anomalydetect/data/www_figs/newfigs/';
end
function out = WWW_DIR_LOCAL_REPO
out = '~/Documents/anomalydetect/data/www_figs/';
end

