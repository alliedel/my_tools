function fnm = publish_www(file, options, varargin)

if ~exist('options','var')
    fnm = publish(file,'html');
elseif ~exist('varargin','var') || isempty(varargin)
       options.format = 'html';
    fnm = publish(file,options);
else
    options.format = 'html';
    fnm = publish(file,options,varargin{:});
end

[fldr,nm,~] = fileparts(fnm);
str_publishToSite = sprintf('scp -r %s adelgior@yell.ius.cs.cmu.edu:~/www/research_results/%s',fldr,nm);
str_publishToSite2 = sprintf('scp ~/Documents/anomalydetect/data/www_figs/newfigs/*.png adelgior@yell.ius.cs.cmu.edu:~/www/research_results/www_figs/');
% str_publishToSite3 = sprintf('scp ~/Documents/anomalydetect/data/www_figs/newfigs/*.jpg adelgior@yell.ius.cs.cmu.edu:~/www/research_results/www_figs/');
str_publishToSite4 = sprintf('mv ~/Documents/anomalydetect/data/www_figs/newfigs/*.png ~/Documents/anomalydetect/data/www_figs/');
% str_publishToSite5 = sprintf('mv ~/Documents/anomalydetect/data/www_figs/newfigs/*.jpg ~/Documents/anomalydetect/data/www_figs/');
system(str_publishToSite);
system(str_publishToSite2);
% system(str_publishToSite3);
system(str_publishToSite4);
% system(str_publishToSite5);

end
