function str = displayInHTML(var, ic)

% ic = indent count.  By default, set to 0
if ~exist('indent','var')
    ic = 0;
end

if isstruct(var)
    str = structInHTML(var, ic);
elseif ischar(var)
    str = stringInHTML(var, ic);
elseif isscalar(var)
    str = scalarInHTML(var, ic);
elseif isnumeric(var) && isvector(var)
    str = numericArrayInHTML(var, ic);
else
    display(var)
    error('I don''t know how to display this in HTML.');
end

end

function str = structInHTML(s, start_indCnt)
    str = [ind(start_indCnt) '<ul>\n'];
    ic = start_indCnt + 1;
    fs = fieldnames(s);
    for i = 1:length(fs)
        fieldnm = fs{i};
        val = s.(fieldnm);
        str = [str, ind(ic) '<li> ' fieldnm ' : ' displayInHTML(val,ic) ' </li>\n'];
    end
    ic = ic - 1;
    str = [str, ind(ic), '</ul>\n'];
end

function str = numericArrayInHTML(n, indent)
    str = [indent '[ ' num2str(n) ' ]'];
end

function str = scalarInHTML(vec, indent)
    str = [indent num2str(vec)];
end

function str = stringInHTML(s, indent)
    str = [indent s];
end

function s = ind(indent_cnt)
    s = repmat(INDENT,indent_cnt);
end

function ret = INDENT % #define INDENT
    ret = '\t';
end
