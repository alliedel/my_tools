function ConvertToLibSVM(fnameIn, fnameOut)
% ConvertToLibSVM(fnameIn, fnameOut)
%fname = 'scene01_all_white_1_kmeans_all_K_50_BOV_hard_histNorm_none_fn.mat';
%addpath(genpath('~/Desktop/anomalydetectClean/code/src/'))

load(fnameIn,'fn','frame_idxs')
if ~exist('frame_idxs','var')
    frame_idxs = (1:size(fn,1))';                    
end
if length(frame_idxs) ~= size(fn,1)
    error('length(frame_idxs) needs to == size(fn,1)\n')
end
[rs,cs] = find(isnan(fn));
fn(rs,:) = [];
frame_idxs(rs) = [];
assert(size(fn,1) == length(frame_idxs));
fprintf('Calling libsvmwrite\n')
libsvmwrite(fnameOut, double(frame_idxs(:)), sparse(double(fn))) % sparse is dumb and hasn't been implemented for singles yet
fprintf('libsvmwrite finished\n');
if existAndNotEmpty(fnameOut) == 0
    if exist(fnameOut,'file')
       system(sprintf('rm %s',fnameOut))
    end
       error('Conversion to LibSVM failed')
end

end 
